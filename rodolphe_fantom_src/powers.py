# File: power.py
# Content: The functions to simulate the use of each character's powers

import copy

from rodolphe_fantom_src.move import move
from rodolphe_fantom_src.positions import all_moves


all_paths = [[0,1], # These are all the paths where the lock can be set
             [0,4],
             [1,2],
             [2,3],
             [3,7],
             [4,5],
             [4,8],
             [5,6],
             [6,7],
             [7,9],
             [8,9]]

# Purple
def invert_location(target_color, game_state):
    game_state_copy = copy.deepcopy(game_state)
    purple_id = 0
    target_id = 0
    for idx, chara in enumerate(game_state['characters']):
        if chara['color'] == 'purple':
            purple_id = idx
        elif chara['color'] == target_color:
            target_id = idx
    save_pos = game_state_copy['characters'][purple_id]['position']
    game_state_copy['characters'][purple_id]['position'] = game_state_copy['characters'][target_id]['position']
    game_state_copy['characters'][target_id]['position'] = save_pos
    return game_state_copy

def purple_power(character, game_state):
    answers = []
    for char in game_state['characters']:
        if char['color'] != 'purple':
            location = char['position']
            game_state_copy = invert_location(char['color'], game_state)
            answers += [move(character, location, game_state_copy, special = char['color'])]
    return answers

# Blue
def blue_power(character, location, game_state):
    # I did not figure a simple strategy for the lock, as it is a very situational tool
    # (compared to grey's power, which can be an instantaneous reward)
    # Because of this, this power has virtually no impact on our calculations
    answers = []
    for path in all_paths:
        game_state_copy = copy.deepcopy(game_state)
        game_state_copy['lock'] = path
        answers += [move(character, location, game_state_copy, special = path)]
    return answers # Returns the highest gain achieved for each power location

# Red
def red_power(character, location, game_state):
    # Red character's power has no direct action over the game's state
    # (except nbr of suspects, and carlotta's location), but it is better to use it than any other simple movement
    return (move(character, location, game_state))

# Grey
def grey_power(character, location, game_state):
    answers = []
    rooms = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
    for room in rooms:
        game_state_copy = game_state
        game_state_copy['shadow'] = room
        answers += [move(character, location, game_state_copy, special = room)]
    return answers # Returns the highest gain achieved for each power location

# Black
def black_power(character, location, game_state):
    game_state_copy = copy.deepcopy(game_state)
    for idx, char in enumerate(game_state_copy['characters']):
        if location in all_moves[char['position']]:
            game_state_copy['characters'][idx]['position'] = location
    return move(character, location, game_state_copy)

# White
def white_power(character, location, game_state):
    return None # While I do not know how the game handles white power, I will consider it worse than anything

# Brown
def brown_power(character, location, game_state):
    answers = []
    for idx, char in enumerate(game_state['characters']):
        if char['position'] == character['position']:
            game_state_copy = copy.deepcopy(game_state)
            game_state_copy['characters'][idx]['position'] = location
            answers += [move(character, location, game_state_copy, special = char['color'])]
    return answers

def moves(character, use_power, location, game_state):
    if use_power == True:
        if character['color'] == 'purple':
            return purple_power(character, game_state) 
            # Exchange places with someone, does not move
        elif character['color'] == 'blue':
            return blue_power(character, location, game_state)
            # We can not choose whether to use the lock or not
        elif character['color'] == 'red':
            return red_power(character, location, game_state) 
            # Draws a card and move
        elif character['color'] == 'grey':
            return grey_power(character, location, game_state)
            # Turns the light off somewhere and moves
        elif character['color'] == 'black':
            return black_power(character, location, game_state)
            # Moves and attracts close character to her
        elif character['color'] == 'white':
            return white_power(character, location, game_state)
            # Moves and repulses other characters 
        elif character['color'] == 'brown':
            return brown_power(character, location, game_state)
            # Moves with someone from the same starting room
    else:
        return move(character, location, game_state, use_power = False)