import config
import numpy as np
from rodolphe_inspector_src.inspector_utils import best_gain_finder, priority_gain_color, get_index
def power_use(selected):
    return (0)

def card_selection(all):
    card_list = all["data"]
    char = all["game state"]["characters"]
    pos = best_gain_finder(card_list, char)
    selected = priority_gain_color(pos)
    config.last_play = selected
    print("\n<============* CARD SELECTION *======================>")
    print("list of card:", card_list)
    print("selected card:", config.last_play)
    return get_index(selected, card_list)