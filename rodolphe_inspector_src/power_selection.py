import config
import numpy
import random
from rodolphe_inspector_src.inspector_utils import room_pop

#RED = INNOCENTE SUSPECT #NO SERVER QUESTION
#WHITE= EXPULSE activate if too much grouped
#PURPLE = put purple in full room EXCHANGE POS with white # ACTIVATE 1 if gain < 8 -> Black too much isolated or White too much grouped 
#GREY = PUT SHADOW in ROOM in empty # GREY CHARACTER POWER
#PINK = SECRET PATH to empty room # No power needed
#BROWN = tAKE A CHARACTER WITH HIM if too much isolated # activate brown power
#BLACK = ATTIRE ALL PERS if too much isolated
#BLUE = LOCK useless

def printy(all):
    print("question type ===>", all["question type"], "\n")
    print("data ===>", all["data"], "\n")
    print("game state ==============================>")
    print("CARLOTTA:", all["game state"]["position_carlotta"], "\n")
    print("EXIT:", all["game state"]["exit"], "\n")
    print("NUM_TOUR:", all["game state"]["num_tour"], "\n")
    print("SHADOW:", all["game state"]["shadow"], "\n")
    print("BLOCKED:", all["game state"]["blocked"], "\n")
    print("CHARACTER STATE:", all["game state"]["characters"], "\n")
    print("CHARACTER CARD:", all["game state"]["character_cards"], "\n")
    print("ACTIVE CARD:", all["game state"]["active character_cards"], "\n")
    print("<=======================================================>" + "\n")

def grp_ratio(room):
    isolated, grouped = 0,0
    for x in room:
        if x == 1:
            isolated += x
        elif x > 1:
            grouped += x
    if grouped > isolated:
        return "grouped"
    else:
        return "isolated"

def power_parser(all):
    print("<============* POWER SELECTION *===================>")
    question = all["question type"]
    print("Question:", question)
    f_word = question.split()[0]
    if f_word == "activate":
        print("Data:", all["data"])
        return activate_power(all)
    else:
        print("NData:",all["data"])
        return power_character(all, f_word)

def purple_power(all):
    if int(config.last_play["gain"]) == 8:
        return 0
    else:
        ratio = grp_ratio(room_pop(all["game state"]["characters"]))
        if ratio == "grouped":
            print("coucou")
            config.last_play["data"] = "white"
            return (1)
        else:
            print("coucou1")
            config.last_play["data"] = "black"
            return (1)

def white_power(all):
    ratio = grp_ratio(room_pop(all["game state"]["characters"]))
    if ratio == "grouped":
        config.last_play["data"] = 0
        return (1)
    else:
        return (0)

def brown_power(all):
    ratio = grp_ratio(room_pop(all["game state"]["characters"]))
    if ratio == "grouped":
        return (0)
    else:
        return (1)

def black_power(all):
    ratio = grp_ratio(room_pop(all["game state"]["characters"]))
    if ratio == "grouped":
        return (1)
    else:
        return (0)

def activate_power(all):
    color = config.last_play["color"]
    if color == "purple":
        return purple_power(all)
    if color == "white":
        return white_power(all)
    if color == "blue":
        return (1)
    if color == "brown":
        return brown_power(all)
    if color == "black":
        return black_power(all)

def color_index(all):
    data = all["data"]
    for idx, x in enumerate(data):
        if config.last_play["data"] == x:
            return idx

def power_character(all, f_word):
    color = config.last_play["color"]
    if color == "purple":
        return color_index(all)
    if color == "white":
        return config.last_play["data"]
    return random.randint(0, len(all["data"])-1)
