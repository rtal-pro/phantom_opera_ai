from rodolphe_inspector_src.card_selection import card_selection
from rodolphe_inspector_src.inspector_position_selection import position_selection
from rodolphe_inspector_src.power_selection import power_parser


def question_parser(all):
    question = all["question type"]
    if question == "select character":
        return card_selection(all)
    elif question == "select position":
        return position_selection(all)
    else:
        return power_parser(all)
        