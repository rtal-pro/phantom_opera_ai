# File: Move.py
# Content: The functions used to calculate the effect of each move on the game's state

import copy

from rodolphe_fantom_src.positions import get_rooms_state

answer_dict = {
    'character': None,
    'power_use': None,
    'position': None,
    'gain': None,
    'special': None,
}

def get_fantom_state(game_state):
    rooms_state = get_rooms_state(game_state)
    fantom_color = game_state['fantom']
    for character in game_state['characters']:
        if character['color'] == fantom_color:
            fantom_location = character['position']
    if game_state['shadow'] == fantom_location or rooms_state[fantom_location] == 1:
        return 'ISOLATED'
    elif rooms_state[fantom_location] > 1:
        return 'GROUPED'
    else:
        return 'ERROR'

def get_nbr_grouped(game_state):
    rooms_state = get_rooms_state(game_state)
    result = 0
    for idx, room in enumerate(rooms_state):
        if room > 1 and game_state['shadow'] != idx:
            result += room
    return result

def get_nbr_isolated(game_state):
    rooms_state = get_rooms_state(game_state)
    result = 0
    for idx, room in enumerate(rooms_state):
        if room == 1 or game_state['shadow'] == idx:
            result += 1
    return result

def move(character, location, game_state, special = None, use_power = True):
    game_state_copy = copy.deepcopy(game_state)
    for idx, char in enumerate(game_state['characters']):
        if char['color'] == character['color']:
            game_state_copy['characters'][idx]['position'] = location
            break;
    
    fantom_state = get_fantom_state(game_state_copy)
    nbr_grouped = get_nbr_grouped(game_state_copy)
    nbr_isolated = get_nbr_isolated(game_state_copy)
    gain = nbr_grouped - nbr_isolated if fantom_state == 'GROUPED' else nbr_isolated - nbr_grouped

    if character['color'] == 'red':
        gain += 2 # Small boost to red's gain to softly prioritize it over other characters

    answer = copy.deepcopy(answer_dict)
    answer['character'] = character['color']
    answer['position'] = location
    answer['power_use'] = use_power
    answer['gain'] = gain
    answer['special'] = special
    return (answer)