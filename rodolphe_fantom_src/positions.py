# File: Positions.py
# Content: The functions to calculate the possible movements a character can make in a turn

all_moves = [[1, 4], # Room 0
             [0, 2], # Room 1
             [1, 3], # Room 2
             [2, 7], # Room 3
             [0, 5, 8], # Room 4
             [4, 6], # Room 5
             [5, 7], # Room 6
             [3, 6, 9], # Room 7
             [4, 9], # Room 8
             [7, 8]] # Room 9

# Movements for the pink character
pink_movements = [[1, 4], # Room 0
                  [0, 2, 5, 7], # Room 1
                  [1, 3, 6], # Room 2
                  [2, 7], # Room 3
                  [0, 5, 8, 9], # Room 4
                  [4, 6, 1, 8], # Room 5
                  [5, 7, 2, 9], # Room 6
                  [3, 6, 9, 1], # Room 7
                  [4, 9, 5], # Room 8
                  [7, 8, 4, 6]] # Room 9

def get_rooms_state(game_state):
    rooms = [0] * 10
    for character in game_state['characters']:
        if character['position'] < 0 or character['position'] > 9:
            exit
        else:
            rooms[character['position']] += 1
    return rooms

def character_range_recurs(movement_lib, location, n, lock_ation):
    possible = movement_lib[location].copy()
    lock = lock_ation.copy()
    if location in lock:
        lock.remove(location)
        if lock[0] in possible:
            possible.remove(lock[0])
    if (n < 0):
        print(f'There was a bug in the range prediction')
    if n <= 0:
        return possible
    else:
        result = possible
        for destination in possible:
            result = result + character_range_recurs(movement_lib, destination, n - 1, lock_ation)
        return result

def character_range(character, game_state, force_range_one = False):
    # returns a list containing each of the possible location
    # a character can move to
    character_color = character['color']
    character_location = character['position']
    rooms_state = get_rooms_state(game_state)
    lock_ation = game_state['blocked']
    number_of_moves = rooms_state[character_location] if not force_range_one else 1
    movement_lib = pink_movements if character_color == 'pink' else all_moves
    movements = character_range_recurs(movement_lib, character_location, number_of_moves - 1, lock_ation)
    movements = list(set(movements))
    if character_location in movements:
        movements.remove(character_location)
    return(movements)