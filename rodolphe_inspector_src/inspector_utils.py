import config
import numpy as np

def priority_gain_color(plays):
    best_plays = []
    mid_plays = []
    worst_plays = []
    low_plays = []
    for x in plays:
        if x["gain"] == 8:
            best_plays.append(x)
        elif x["gain"] == 6:
            mid_plays.append(x)
        elif x["gain"] == 4:
            low_plays.append(x)
        else:
            worst_plays.append(x)
    if len(best_plays) > 0:
        for x in config.priority:
            for idy, y in enumerate(best_plays):
                if x == y["color"]:
                    return best_plays[idy]
    elif len(mid_plays) > 0:
        for x in config.priority:
            for idy, y in enumerate(mid_plays):
                if x == y["color"]:
                    return mid_plays[idy]
    elif len(low_plays) > 0:
        for x in config.priority:
            for idy, y in enumerate(low_plays):
                if x == y["color"]:
                    return low_plays[idy]
    else:
        return worst_plays[0]

def get_index(play, card_list):
    for idx, x in enumerate(card_list):
        if play["color"] == x["color"]:
            return idx

def gain_calculator(room):
    isolated, grouped, gain = 0,0,0
    for x in room:
        if x == 1:
            isolated += x
        elif x > 1:
            grouped += x
    gain = 8 - abs(isolated - grouped)
    """ print("iso:grp:gain:room =>", isolated, grouped, gain, room) """
    return gain

def room_pop(char):
    room = np.zeros(10)
    for x in char:
        room[x['position']] += 1
    return room



def best_gain_finder(card, char):
    gains = []
    for actual in card:
        if actual["color"] == "pink":
            for sol in config.pink_path[actual["position"]]:
                tmp = room_pop(char)
                tmp[sol] += 1
                tmp[actual['position']] -= 1
                gain = gain_calculator(tmp)
                gains.append({"color" :actual["color"], "move": sol, "gain": gain, "data": 0})
        else:
            for sol in config.path[actual["position"]]:
                tmp = room_pop(char)
                tmp[sol] += 1
                tmp[actual['position']] -= 1
                gain = gain_calculator(tmp)
                gains.append({"color" :actual["color"], "move": sol, "gain": gain, "data": 0})
    return gains