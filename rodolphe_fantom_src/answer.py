# File: Answer.py
# Content: The functions used to get our list of possible answers

import copy

from rodolphe_fantom_src.positions import character_range
from rodolphe_fantom_src.powers import moves

def flatten(toflatten):
    result = []
    for item in toflatten:
        if isinstance(item, list):
            result += flatten(item)
        elif item != None:
            result.append(item)
    return result

def movement(character, use_power, data, game_state):
    answers = []
    for location in data:
        answers += [moves(character, use_power, location, game_state)]
    return(answers)


def power_use(character, data, game_state):
    answers = []
    character_possible_movements = character_range(character, game_state)
    for power in data:
        use_power = False if power == 0 else True
        answers += movement(character, use_power, character_possible_movements, copy.deepcopy(game_state))
    return(answers)

def character(data, game_state):
    answers = []
    power_choice = [0,1]
    for character in data:
        power_choice = [0,1]
        if character['color'] == 'blue' or character['color'] == 'grey' or character['color'] == 'red':
            power_choice = [1] # Blue, Grey and Red do not have any choice and will always use their power
        if character['color'] == 'pink':
            power_choice = [0] # Pink can not activate its power as it is a passive
        answers += power_use(character, power_choice, copy.deepcopy(game_state))
    flat_answers = flatten(answers)
    return(flat_answers)


def get_answers(data, game_state):
    answers = character(data, game_state)
    return(answers)
