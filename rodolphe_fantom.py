import json
import logging
import os
import socket
from logging.handlers import RotatingFileHandler
import protocol
from rodolphe_fantom_src.answer import get_answers

host = "localhost"
port = 12000
# HEADERSIZE = 10

"""
set up fantom logging
"""
logger = logging.getLogger()
logger.setLevel(logging.DEBUG)
formatter = logging.Formatter(
    "%(asctime)s :: %(levelname)s :: %(message)s", "%H:%M:%S")
# file
if os.path.exists("./logs/fantom.log"):
    os.remove("./logs/fantom.log")
file_handler = RotatingFileHandler('./logs/fantom.log', 'a', 1000000, 1)
file_handler.setLevel(logging.DEBUG)
file_handler.setFormatter(formatter)
logger.addHandler(file_handler)
# stream
stream_handler = logging.StreamHandler()
stream_handler.setLevel(logging.WARNING)
logger.addHandler(stream_handler)


class Player():

    def __init__(self):

        self.end = False
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

        self.answ = None

    def reset_save(self):
        self.answ = None

    def question_logger(self, question):
        turn = question['game state']['num_tour']
        question_type = question['question type']
        data = question['data']
        carlotta = question['game state']['position_carlotta']
        shadow = question['game state']['shadow']
        blocked = question['game state']['blocked']
        characters = question['game state']['characters']
        cards = question['game state']['active character_cards']

        logger.debug(f'\nTour number -> {turn}')
        logger.debug(f'ALL DATA ----> {question}')
        logger.debug(f'Question ----> {question_type}')
        logger.debug(f'Data --------> {data}')
        logger.debug(f'Carlotta ----> {carlotta}')
        logger.debug(f'Shadow ------> {shadow}')
        logger.debug(f'Lock --------> {blocked}')
        logger.debug(f'Characters --> {characters}')
        logger.debug(f'Cards -------> {cards}')

    def select_answer(self, answers):
        list_best = []
        gain_max = -20
        for answer in answers:
            if answer['gain'] > gain_max:
                gain_max = answer['gain']
        for answer in answers:
            if answer['gain'] == gain_max:
                list_best.append(answer)
        for answer in list_best:
            if answer['character'] == 'red':
                self.answ = answer # Prioritize red character against others if they have an equal gain
                break
            else:
                self.answ = list_best[0]
                break
    
    def get_answer(self, question):
        data = question['data']
        game_state = question['game state']

        self.reset_save()
        answers = get_answers(data, game_state)
        self.select_answer(answers)

    def character_index(self, data):
        for idx, character in enumerate(data):
            if character['color'] == self.answ['character']:
                return(idx)
        print('Character selection: No match found')
        return(0)
    
    def position_index(self, data):
        for idx, position in enumerate(data):
            if position == self.answ['position']:
                return(idx)
        print('Position selection: No match found')
        return(0)

    def power_index(self):
        if self.answ['power_use']:
            return 1
        else:
            return 0

    def special_index(self, data):
        for idx, special in enumerate(data):
            if isinstance(self.answ['special'], list):
                if special == self.answ['special'][0]:
                    self.answ['special'] = self.answ['special'][1]
                    return(idx)
            if special == self.answ['special']:
                return(idx)
        print(f'Special selection: No match found.\nData:{data}\nAnswer:{self.answ}')
        return 0

    def question_parser(self, question):
        question_type = question['question type']
        data = question['data']
        response_index = 0
        if question_type == 'select character':
            self.get_answer(question)
            response_index = self.character_index(data)
        elif question_type == 'select position':
            response_index = self.position_index(data)
        elif 'activate' in question_type:
            response_index = self.power_index()
        elif 'character power' in question_type:
            response_index = self.special_index(data)
        else:
            response_index = 0
        
        return response_index

    def connect(self):
        self.socket.connect((host, port))

    def reset(self):
        self.socket.close()

    def answer(self, question):
        self.question_logger(question)
        response_index = self.question_parser(question)
        return response_index

    def handle_json(self, data):
        data = json.loads(data)
        response = self.answer(data)
        # send back to server
        bytes_data = json.dumps(response).encode("utf-8")
        protocol.send_json(self.socket, bytes_data)

    def run(self):

        self.connect()

        while self.end is not True:
            received_message = protocol.receive_json(self.socket)
            if received_message:
                self.handle_json(received_message)
            else:
                print("no message, finished learning")
                self.end = True


p = Player()

p.run()
