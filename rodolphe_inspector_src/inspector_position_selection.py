import config
import numpy

def best_pos_index(room):
    for idx, x in enumerate(room):
        if config.last_play["move"] == x:
            return idx

def position_selection(all):
    data = all["game state"]["characters"]
    room = all["data"]
    print("<============* POSITION SELECTION *===================>")
    print("question type ===>", all["question type"])
    print("Actual player:", config.last_play)
    print("ROOM proposed ===>", all["data"])
    return best_pos_index(room)